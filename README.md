Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?
    * They are differnet because the logger.properties specifies for the logger.log level to be ALL, while the ConsoleHandler's level is INFO
1.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
    * This comes from the logger inside of the ConditionEvaluator class inside of JUnit. The level is set to FINER, and since our logger.log level is set to ALL, we see this message.
1.  What does Assertions.assertThrows do?
    * Asserts that a specified Exception was thrown from the specified lambda.
1.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)
        * We can define this so our classes can be (de)serializable. `java.io.Serializable` uses the serialVersionUID when deserializing to verify that sender and receivers have classes that are compatible with respect to serialization. If the ID is different than the sender's, deserialization results in InvalidClassException.
    2.  Why do we need to override constructors?
        * The compiler will will not generate these for us, because it will only generate a default constructor.
    3.  Why we did not override other Exception methods?	
        * We do not need to override anything else because we we did not need to change the supertype's method behavior.
1.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)
    * Static blocks are executed when the program first begins running, before other code and tests. It is typically used for initialization, and it also is in our case - initializing the static logger variable.
1.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
    * README.md describes the purpose of a repository and gives information a user may need to know about a repository. the .md extension denotes that it is a Markdown file (used for formatting text). It is related to bitbucket, github etc because they are used for storing repositories which typically have a README.md.
1.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)
    * The test is failing due to the finally {..} block inside of the timeMe() method. The exception isn't thrown yet and JUnit fails the test once it starts executing the finally block. Moving log statements outside of the finally block, and removing the finally block fixes the tests to pass.
1.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)
    * The issue is that the exception is not thrown until after the finally block is executed, but JUnit fails the test once the finally block starts being executed (before the exception is thrown)
1.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 
    * see bitbucket repo at the bottom - file `junit-plugin-run.PNG`
1.  Make a printScreen of your eclipse Maven test run, with console
    * see bitbucket repo at the bottom - file `maven-test-run.PNG`
1.  What category of Exceptions is TimerException and what is NullPointerException
    * TimerException is a checked exception, and NullPointerException is an unchecked exception.
1.  Push the updated/fixed source code to your own repository.
    * https://bitbucket.org/nnazo/exceptionrunner/src/master/